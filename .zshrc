## Increase history size
export HISTSIZE=1000000000
export SAVEHIST=$HISTSIZE
setopt EXTENDED_HISTORY

## Use bash values
source ~/.aliases

## Prompt
PS1='%B%F{240}%2~%f%b %# '

## From brew output with zsh-completions (Oh-My-ZSH handles?)
#if type brew &>/dev/null; then
#    FPATH=$(brew --prefix)/share/zsh-completions:$FPATH
#
#    autoload -Uz compinit
#    compinit
#fi
